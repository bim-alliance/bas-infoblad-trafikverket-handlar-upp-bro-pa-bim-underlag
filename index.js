var infopack = require('infopack');

/**
 * Load generators below
 */
// var infopackGenMarkdownToHtml = require('infopack-gen-markdown-to-html');

/**
 * Create a new pipeline
 */
var pipeline = new infopack.Pipeline();

/**
 * Add steps below
 */
// pipeline.addStep({...});

/**
 * Run pipeline
 */
pipeline
    .run()
    .then((data) => {
        return pipeline.generateStaticPage();
    })
    .then(() => {
        console.log(pipeline.prettyTable());
    })
    .catch((err) => {
        console.error(err);
    });
